import * as THREE from 'three'
import Experience from "./Experience.js"
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { gsap } from 'gsap'

export default class Camera {
    constructor() {
        this.experience = new Experience()
        this.sizes = this.experience.sizes
        this.scene = this.experience.scene
        this.canvas = this.experience.canvas
        this.debug = this.experience.debug
        this.world = this.experience.world

        this.setInstance()
        this.setControls()
    }

    setInstance() {
        this.instance = new THREE.PerspectiveCamera(
            55,
            this.sizes.width / this.sizes.height,
            0.1,
            2000
        )
        this.scene.add(this.instance)
    }

    setControls() {
        this.controls = new OrbitControls(this.instance, this.canvas)
        this.controls.enableDamping = true
    }

    resize() {
        this.instance.aspect = this.sizes.width / this.sizes.height
        this.instance.updateProjectionMatrix()
    }

    update() {
        this.controls.update()
    }

    changePosition(position, cameraPosition) {

        const duration = 2
        gsap.to(this.instance.position,
            {
                x: position.x + cameraPosition.x,
                y: position.y + cameraPosition.y,
                z: position.z + cameraPosition.z,
                duration: duration,
                ease: 'power3.inOut'
            })

        gsap.to(this.controls.target,
            {
                x: position.x,
                y: position.y,
                z: position.z,
                duration: duration,
                ease: 'power3.inOut'
            })
    }

}