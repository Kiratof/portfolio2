import * as THREE from 'three'
import Sizes from "./Utils/Sizes.js"
import Time from "./Utils/Time.js"
import Camera from './Camera.js'
import Renderer from './Renderer.js'
import World from './World/World.js'
import Ressources from './Utils/Ressources.js'
import sources from './sources.js'
import Debug from './Utils/Debug.js'
import dbWorld from './Database/dbWorld.js'

let instance = null

export default class Experience {
    constructor(canvas) {

        if (instance) {
            return instance
        }
        instance = this

        //Global access
        window.experience = this

        //Options
        this.canvas = canvas

        //Setup
        this.debug = new Debug()
        this.sizes = new Sizes()
        this.time = new Time()
        this.scene = new THREE.Scene()
        this.ressources = new Ressources(sources)
        this.camera = new Camera()
        this.renderer = new Renderer()
        this.world = new World()

        // Debug
        if (this.debug.active) {
            this.debugFolder = this.debug.ui.addFolder('camera')
        }
        if (this.debug.active) {
            const debugObject = {
                Intro: () => {

                    this.camera.changePosition(dbWorld['INTRO'].position, dbWorld['INTRO'].cameraPosition)

                    dbWorld['INTRO'].active = true
                    dbWorld['PROJECTS'].active = false
                    dbWorld['INFORMATIONS'].active = false

                    //Remove html point TODO REFACTOR
                    this.world.projects.projects.forEach(project => {
                        project.htmlPoint.element.classList.remove('visible')
                    });

                    this.world.informations.informations.forEach(information => {
                        information.cta.element.classList.remove('visible')
                    });
                },
                Projects: () => {

                    this.camera.changePosition(dbWorld['PROJECTS'].position, dbWorld['PROJECTS'].cameraPosition)

                    dbWorld['INTRO'].active = false
                    dbWorld['PROJECTS'].active = true
                    dbWorld['INFORMATIONS'].active = false


                    this.world.informations.informations.forEach(information => {
                        information.cta.element.classList.remove('visible')
                    });
                },
                Informations: () => {

                    this.camera.changePosition(dbWorld['INFORMATIONS'].position, dbWorld['INFORMATIONS'].cameraPosition)

                    dbWorld['INTRO'].active = false
                    dbWorld['PROJECTS'].active = false
                    dbWorld['INFORMATIONS'].active = true

                    //Remove html point TODO REFACTOR
                    this.world.projects.projects.forEach(project => {
                        project.htmlPoint.element.classList.remove('visible')
                    });

                    this.world.informations.informations.forEach(information => {
                        information.cta.element.classList.add('visible')
                    });


                },
            }

            this.debugFolder.add(debugObject, 'Intro')
            this.debugFolder.add(debugObject, 'Projects')
            this.debugFolder.add(debugObject, 'Informations')
        }

        //Sizes resize event
        this.sizes.on('resize', () => {
            this.resize()
        })

        //Time tick event
        this.time.on('tick', () => {
            this.update()
        })

        this.camera.changePosition(dbWorld['INTRO'].position, dbWorld['INTRO'].cameraPosition)

    }

    resize() {
        this.camera.resize()
        this.renderer.resize()
    }

    update() {
        this.camera.update()
        this.world.update()
        this.renderer.update()
    }

    destroy() {
        this.sizes.off('resize')
        this.time.off('tick')

        // Traverse the whole scene
        this.scene.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                child.geometry.dispose()

                for (const key in child.material) {
                    const value = child.material[key]

                    if (value && typeof value.dispose === 'function') {
                        value.dispose()
                    }
                }
            }
        })

        this.camera.controls.dispose()
        this.renderer.instance.dispose()

        if (this.debug.active) {
            this.debug.ui.destroy()
        }
    }
}

