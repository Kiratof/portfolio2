export default [
    // TEXTURES

    // MODELS
    {
        name: 'guyModel',
        type: 'gltfModel',
        path: 'models/Guy/guy2.gltf'
    },
    {
        name: 'birdModel',
        type: 'gltfModel',
        path: 'models/Bird/scene.gltf'
    },
    {
        name: 'boatModel',
        type: 'gltfModel',
        path: 'models/Fishing_boat/scene.gltf'
    },
    {
        name: 'palmTreeModel',
        type: 'gltfModel',
        path: 'models/Palm/palm.glb'
    },
    {
        name: 'projectScene',
        type: 'gltfModel',
        path: 'models/ProjectsLibrary/beach.glb'
    },

    // FONT
    {
        name: 'titleFont',
        type: 'font',
        path: 'fonts/helvetiker_regular.typeface.json'
    }
]