/* COLOR PALETTE */
const dbColors = {
    LemonChiffon: { name: "Lemon chiffon", hex: "FEFAD4", rgb: [254, 250, 212], cmyk: [0, 2, 17, 0], hsb: [54, 17, 100], hsl: [54, 95, 91], lab: [98, -5, 19] },
    Jasmine: { name: "Jasmine", hex: "FEE298", rgb: [254, 226, 152], cmyk: [0, 11, 40, 0], hsb: [44, 40, 100], hsl: [44, 98, 80], lab: [91, 0, 40] },
    Orange: { name: "Orange (web)", hex: "FDB030", rgb: [253, 176, 48], cmyk: [0, 30, 81, 1], hsb: [37, 81, 99], hsl: [37, 98, 59], lab: [77, 18, 71] },
    Pumpkin: { name: "Pumpkin", hex: "FA7725", rgb: [250, 119, 37], cmyk: [0, 52, 85, 2], hsb: [23, 85, 98], hsl: [23, 96, 56], lab: [65, 46, 64] },
    CaputMortuum: { name: "Caput mortuum", hex: "6A2D1E", rgb: [106, 45, 30], cmyk: [0, 58, 72, 58], hsb: [12, 72, 42], hsl: [12, 56, 27], lab: [27, 26, 23] },
    PayneGray: { name: "Payne's gray", hex: "186380", rgb: [24, 99, 128], cmyk: [81, 23, 0, 50], hsb: [197, 81, 50], hsl: [197, 68, 30], lab: [39, -12, -23] },
    MyrtleGreen: { name: "Myrtle Green", hex: "42807D", rgb: [66, 128, 125], cmyk: [48, 0, 2, 50], hsb: [177, 48, 50], hsl: [177, 32, 38], lab: [50, -21, -5] },
    Turquoise: { name: "Turquoise", hex: "35D0B1", rgb: [53, 208, 177], cmyk: [75, 0, 15, 18], hsb: [168, 75, 82], hsl: [168, 62, 51], lab: [75, -47, 4] },
    AppleGreen: { name: "Apple green", hex: "91A02B", rgb: [145, 160, 43], cmyk: [9, 0, 73, 37], hsb: [68, 73, 63], hsl: [68, 58, 40], lab: [63, -21, 56] },
    Ebony: { name: "Ebony", hex: "6E7263", rgb: [110, 114, 99], cmyk: [4, 0, 13, 55], hsb: [76, 13, 45], hsl: [76, 7, 42], lab: [47, -4, 8] }
}

export default dbColors
