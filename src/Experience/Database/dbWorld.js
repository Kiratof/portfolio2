import * as THREE from "three"

const dbWorld = {}

//INTRO
dbWorld['INTRO'] = {}
dbWorld['INTRO'].name = 'intro'
dbWorld['INTRO'].position = new THREE.Vector3(0, 0, 0)
dbWorld['INTRO'].active = true
dbWorld['INTRO'].cameraPosition = new THREE.Vector3(0, 5, 35)


//PROJECT
dbWorld['PROJECTS'] = {}
dbWorld['PROJECTS'].name = 'projects'
dbWorld['PROJECTS'].position = new THREE.Vector3(-200, 10, 70)
dbWorld['PROJECTS'].active = false
dbWorld['PROJECTS'].cameraPosition = new THREE.Vector3(3, 3, -22)

dbWorld['PROJECTS'].projects = []
let projectId = 1
dbWorld['PROJECTS'].projects['gkps3'] = {}
dbWorld['PROJECTS'].projects['gkps3'].id = projectId++
dbWorld['PROJECTS'].projects['gkps3'].name = 'GKPS3'
dbWorld['PROJECTS'].projects['gkps3'].type = 'gamejam'
dbWorld['PROJECTS'].projects['gkps3'].description = 'Game jam - Not a Jam'
dbWorld['PROJECTS'].projects['gkps3'].position = { x: 2.5, y: 2.7, z: -3 }

dbWorld['PROJECTS'].projects['nottoday'] = {}
dbWorld['PROJECTS'].projects['nottoday'].id = projectId++
dbWorld['PROJECTS'].projects['nottoday'].name = 'Not Today'
dbWorld['PROJECTS'].projects['nottoday'].type = 'gamejam'
dbWorld['PROJECTS'].projects['nottoday'].description = 'Game jam - GMTK 2023'
dbWorld['PROJECTS'].projects['nottoday'].position = { x: -2.5, y: -0.5, z: -3 }

dbWorld['PROJECTS'].projects['robot'] = {}
dbWorld['PROJECTS'].projects['robot'].id = projectId++
dbWorld['PROJECTS'].projects['robot'].name = 'RoboT'
dbWorld['PROJECTS'].projects['robot'].type = 'gamejam'
dbWorld['PROJECTS'].projects['robot'].description = 'Game jam - Global game jam'
dbWorld['PROJECTS'].projects['robot'].position = { x: 3, y: -3.5, z: -3 }


//INFORMATIONS
dbWorld['INFORMATIONS'] = {}
dbWorld['INFORMATIONS'].name = 'informations'
dbWorld['INFORMATIONS'].position = new THREE.Vector3(300, 10, 70)
dbWorld['INFORMATIONS'].active = false
dbWorld['INFORMATIONS'].cameraPosition = new THREE.Vector3(15, 5, -100)


dbWorld['INFORMATIONS'].informations = []
dbWorld['INFORMATIONS'].informations['itchio'] = {}
dbWorld['INFORMATIONS'].informations['itchio'].title = 'Itchio'
dbWorld['INFORMATIONS'].informations['itchio'].outlink = 'https://chaotic-monkey.itch.io/'
dbWorld['INFORMATIONS'].informations['itchio'].informationsOffset = { x: -50, y: 0, z: 0 }

dbWorld['INFORMATIONS'].informations['discord'] = {}
dbWorld['INFORMATIONS'].informations['discord'].title = 'Discord'
dbWorld['INFORMATIONS'].informations['discord'].outlink = 'https://discord.com/users/208240042123067393'
dbWorld['INFORMATIONS'].informations['discord'].informationsOffset = { x: 50, y: 0, z: 0 }

dbWorld['INFORMATIONS'].informations['mail'] = {}
dbWorld['INFORMATIONS'].informations['mail'].title = 'Mail'
dbWorld['INFORMATIONS'].informations['mail'].outlink = 'mailto:christopher@chaotic-monkey.fr'
dbWorld['INFORMATIONS'].informations['mail'].informationsOffset = { x: 0, y: 0, z: 0 }

export default dbWorld
