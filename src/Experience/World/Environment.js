import * as THREE from 'three'
import Experience from "../Experience"
import { Water } from 'three/addons/objects/Water.js';
import { Sky } from 'three/addons/objects/Sky.js';

export default class Environment {
    constructor() {
        this.experience = new Experience()
        this.renderer = this.experience.renderer
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources
        this.debug = this.experience.debug

        // Debug
        if (this.debug.active) {
            this.debugFolder = this.debug.ui.addFolder('Environment')
        }

        this.setSunLight()
        this.setEnvironmentMap()
    }

    setSunLight() {
        this.sunLight = new THREE.DirectionalLight('#ffffff', 4)
        this.sunLight.castShadow = true
        this.sunLight.shadow.camera.far = 15
        this.sunLight.shadow.mapSize.set(1024, 1024)
        this.sunLight.shadow.normalBias = 0.05
        this.sunLight.position.set(0, 1, -5)
        this.scene.add(this.sunLight)

        // Debug
        if (this.debug.active) {
            this.debugFolder
                .add(this.sunLight, 'intensity')
                .name('sunLightIntensity')
                .min(0)
                .max(10)
                .step(0.001)

            this.debugFolder
                .add(this.sunLight.position, 'x')
                .name('sunLightX')
                .min(-5)
                .max(5)
                .step(0.001)

            this.debugFolder
                .add(this.sunLight.position, 'y')
                .name('sunLightY')
                .min(-5)
                .max(5)
                .step(0.001)

            this.debugFolder
                .add(this.sunLight.position, 'z')
                .name('sunLightZ')
                .min(-5)
                .max(5)
                .step(0.001)
        }
    }

    setEnvironmentMap() {
        // Sun
        let sun = new THREE.Vector3();

        // Water
        const waterGeometry = new THREE.PlaneGeometry(1000, 1000);
        this.water = new Water(
            waterGeometry,
            {
                textureWidth: 512,
                textureHeight: 512,
                waterNormals: new THREE.TextureLoader().load('textures/waternormals.jpg', function (texture) {

                    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;

                }),
                sunDirection: new THREE.Vector3(),
                sunColor: 0xffffff,
                waterColor: 0x001e0f,
                distortionScale: 3.7,
                fog: this.scene.fog !== undefined
            }
        );

        this.water.rotation.x = - Math.PI / 2;
        this.water.position.y -= 1
        this.water.position.z -= 450
        this.scene.add(this.water);

        // Skybox
        this.sky = new Sky();
        this.sky.scale.setScalar(10000);
        this.scene.add(this.sky);

        const skyUniforms = this.sky.material.uniforms;

        skyUniforms['turbidity'].value = 10;
        skyUniforms['rayleigh'].value = 2;
        skyUniforms['mieCoefficient'].value = 0.005;
        skyUniforms['mieDirectionalG'].value = 0.8;

        const parameters = {
            elevation: 0.01,
            azimuth: 180
        };




        this.environmentMap = {}
        this.environmentMap.intensity = 0.4
        this.environmentMap.texture = this.sky.material.texture;

        // const pmremGenerator = new THREE.PMREMGenerator(this.renderer);
        // let renderTarget;
        this.environmentMap.updateSun = () => {

            const phi = THREE.MathUtils.degToRad(90 - parameters.elevation)
            const theta = THREE.MathUtils.degToRad(parameters.azimuth)

            sun.setFromSphericalCoords(1, phi, theta)

            this.sky.material.uniforms['sunPosition'].value.copy(sun)
            this.water.material.uniforms['sunDirection'].value.copy(sun).normalize()

            // if (renderTarget !== undefined) renderTarget.dispose()

            // renderTarget = pmremGenerator.fromScene(this.sky)

            this.scene.environment = this.environmentMap.texture

        }

        this.environmentMap.updateMaterials = () => {
            this.scene.traverse((child) => {
                if (child instanceof THREE.Mesh && child.material instanceof THREE.MeshStandardMaterial) {
                    child.material.envMap = this.environmentMap.texture
                    child.material.envMapIntensity = this.environmentMap.intensity
                    child.material.needsUpdate = true
                }
            })
        }

        this.environmentMap.updateSun()
        this.environmentMap.updateMaterials()

        // Debug
        if (this.debug.active) {
            this.debugFolder
                .add(this.environmentMap, 'intensity')
                .name('envMapIntensity')
                .min(0)
                .max(4)
                .step(0.001)
                .onChange(this.environmentMap.updateMaterials)

            const folderSky = this.debug.ui.addFolder('Sky');
            folderSky.add(parameters, 'elevation', 0, 90, 0.01).onChange(this.environmentMap.updateSun);
            folderSky.add(parameters, 'azimuth', - 180, 180, 0.01).onChange(this.environmentMap.updateSun);
            folderSky.open();

            const waterUniforms = this.water.material.uniforms;

            const folderWater = this.debug.ui.addFolder('Water');
            folderWater.add(waterUniforms.distortionScale, 'value', 0, 8, 0.01).name('distortionScale');
            folderWater.add(waterUniforms.size, 'value', 0.1, 10, 0.01).name('size');
            folderWater.open();
        }

    }

    update() {
        this.water.material.uniforms['time'].value += 1.0 / 600.0;
    }

}