import * as THREE from "three"
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import Experience from "../Experience";
import dbColors from "../Database/dbColors";


export default class SectionTitle {
    constructor(text) {

        this.experience = new Experience()
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources
        this.colors = dbColors

        // Set up
        const textGeometry = new TextGeometry(text,
            {
                font: this.experience.ressources.items.titleFont,
                size: 1,
                height: 0.2,
                curveSegments: 6,
                bevelEnabled: true,
                bevelThickness: 0.03,
                bevelSize: 0.02,
                bevelOffset: 0,
                bevelSegments: 4
            }
        )

        textGeometry.center()
        const material = new THREE.MeshBasicMaterial({
            color: new THREE.Color(`#${this.colors.CaputMortuum.hex}`)
        })
        this.mesh = new THREE.Mesh(textGeometry, material)
        this.scene.add(this.mesh)

    }
}