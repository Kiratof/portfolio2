import dbWorld from "../../Database/dbWorld.js"
import Experience from '../../Experience'
import HtmlPoint from './htmlPoint'

export default class Project {
    constructor(name) {

        this.experience = new Experience()
        this.camera = this.experience.camera

        // Database call
        this.project = dbWorld['PROJECTS'].projects[name]

        // Point
        this.htmlPoint = new HtmlPoint(this.project.id, this.project.name)
        this.htmlPoint.position.x = dbWorld['PROJECTS'].position.x + this.project.position.x
        this.htmlPoint.position.y = dbWorld['PROJECTS'].position.y + this.project.position.y
        this.htmlPoint.position.z = dbWorld['PROJECTS'].position.z + this.project.position.z
    }

    update() {
        if (this.htmlPoint) {
            this.htmlPoint.update()
        }
    }
}