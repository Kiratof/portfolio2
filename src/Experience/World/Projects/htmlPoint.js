import * as THREE from 'three'
import Experience from '../../Experience'

export default class HtmlPoint {
    constructor(id, name) {

        this.experience = new Experience()
        this.camera = this.experience.camera
        this.sizes = this.experience.sizes
        this.scene = this.experience.scene

        this.position = new THREE.Vector3()

        this.element = document.createElement('div')
        this.element.setAttribute('class', `point point-${id}`)
        document.body.appendChild(this.element);

        this.label = document.createElement('div')
        this.label.setAttribute('class', 'label')
        this.label.innerText = id
        this.element.appendChild(this.label);

        this.name = document.createElement('div')
        this.name.setAttribute('class', 'text')
        this.name.innerText = name
        this.element.appendChild(this.name);

        /**
         * Raycaster
        */
        this.raycaster = new THREE.Raycaster()
        this.currentIntersect = null

    }

    update() {

        const screenPosition = this.position.clone()
        screenPosition.project(this.camera.instance)

        this.raycaster.setFromCamera(screenPosition, this.camera.instance)
        const intersects = this.raycaster.intersectObjects(this.scene.children, true)

        if (intersects.length === 0) {
            this.element.classList.add('visible')
        } else {
            const intersectionDistance = intersects[0].distance
            const pointDistance = this.position.distanceTo(this.camera.instance.position)

            if (intersectionDistance < pointDistance) {
                this.element.classList.remove('visible')
            } else {
                this.element.classList.add('visible')
            }

        }

        const translateX = screenPosition.x * this.sizes.width * 0.5
        const translateY = - screenPosition.y * this.sizes.height * 0.5
        this.element.style.transform = `translate(${translateX}px, ${translateY}px)`
    }
}