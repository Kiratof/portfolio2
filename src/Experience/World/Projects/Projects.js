import * as THREE from "three"
import Experience from "../../Experience.js"
import SectionTitle from "../SectionTitle.js"
import dbWorld from "../../Database/dbWorld.js"
import Project from "./Project.js"


export default class Projects {
    constructor() {

        this.experience = new Experience()
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources

        // Setup
        this.setTitle()

        this.setProjectScene()

        this.setLighting()

        this.projects = [
            new Project('gkps3'),
            new Project('nottoday'),
            new Project('robot')
        ]
    }

    setTitle() {
        this.title = new SectionTitle('Projects library')
        this.title.mesh.position.copy(dbWorld['PROJECTS'].position)
        this.title.mesh.position.y += 7
        this.title.mesh.position.z -= 3
        this.title.mesh.rotation.y = Math.PI

    }

    setLighting() {

    }

    setProjectScene() {
        this.projectScene = this.ressources.items.projectScene
        this.model = this.projectScene.scene
        this.model.scale.set(4, 4, 4)

        this.model.position.copy(dbWorld['PROJECTS'].position)

        this.model.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                child.castShadow = true
            }
        })

        this.scene.add(this.model)
    }

    update() {
        if (this.projects) {

            this.projects.forEach(project => {
                project.update()
            });
        }
    }
}