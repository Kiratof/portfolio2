import InformationTitle from "./InformationTitle"
import Cta from "./Cta"
import { gsap } from 'gsap'
import dbWorld from "../../Database/dbWorld";

export default class Information {
    constructor(name) {
        this.referencePosition = dbWorld['INFORMATIONS'].position
        this.information = dbWorld['INFORMATIONS'].informations[name]

        // TITLE
        this.title = new InformationTitle(this.information.title)
        this.title.text.position.copy(this.referencePosition)
        this.title.text.position.x += this.information.informationsOffset.x

        // CTA
        this.cta = new Cta(this.information.outlink)
        this.cta.position.x = this.title.text.position.x
        this.cta.position.y = this.title.text.position.y - 10
        this.cta.position.z = this.title.text.position.z
        this.cta.on('mouseEnter', () => {
            gsap.to(this.title.text.rotation, { y: Math.PI * 2, duration: 1, ease: 'power3.inOut' });
        })
        this.cta.on('mouseLeave', () => {
            gsap.to(this.title.text.rotation, { y: 0, duration: 1, ease: 'power3.inOut' });
        })

    }

    update() {
        if (this.cta) {
            this.cta.update()
        }
    }
}