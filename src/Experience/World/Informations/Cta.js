import * as THREE from "three"
import EventEmitter from "../../Utils/EventEmitter"
import Experience from "../../Experience"


export default class Cta extends EventEmitter {
    constructor(outLink) {

        super()

        this.experience = new Experience()
        this.camera = this.experience.camera
        this.sizes = this.experience.sizes

        this.position = new THREE.Vector3()
        this.element = document.createElement('div')
        this.element.setAttribute('class', 'point cta-0')
        document.body.appendChild(this.element);

        this.label = document.createElement('div')
        this.label.setAttribute('class', 'label')
        this.label.innerText = 'textlink'
        this.element.appendChild(this.label);

        this.outLink = outLink

        this.element.addEventListener("mouseenter", (event) => {
            event.target.children[0].style.color = "orange"

            this.trigger('mouseEnter')
        }
        );

        this.element.addEventListener("mouseleave", (event) => {
            event.target.children[0].style.color = "white"

            this.trigger('mouseLeave')
        }
        );

        this.element.addEventListener("click", () => {
            window.open(this.outLink, "_blank");
        });

    }


    update() {
        const screenPosition = this.position.clone()
        screenPosition.project(this.camera.instance)

        const translateX = screenPosition.x * this.sizes.width * 0.5
        const translateY = - screenPosition.y * this.sizes.height * 0.5
        this.element.style.transform = `translate(${translateX}px, ${translateY}px)`
    }
}