import * as THREE from 'three'
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js'
import Experience from '../../Experience.js'
import dbColors from '../../Database/dbColors.js'


export default class InformationTitle {
    constructor(text) {

        this.experiece = new Experience()
        this.scene = this.experiece.scene
        this.ressources = this.experiece.ressources
        this.colors = dbColors

        const textGeometry = new TextGeometry(
            text,
            {
                font: this.ressources.items.titleFont,
                size: 5,
                height: 0.2,
                curveSegments: 6,
                bevelEnabled: true,
                bevelThickness: 0.03,
                bevelSize: 0.02,
                bevelOffset: 0,
                bevelSegments: 4
            }
        )

        textGeometry.center()

        const material = new THREE.MeshBasicMaterial({
            color: new THREE.Color(`#${this.colors.Pumpkin.hex}`)
        })
        this.text = new THREE.Mesh(textGeometry, material)
        this.scene.add(this.text)
    }
}