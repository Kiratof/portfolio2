import SectionTitle from "../SectionTitle"
import Information from "./Information"
import dbWorld from "../../Database/dbWorld"
import Experience from "../../Experience"

export default class Informations {
    constructor() {
        // Setup
        // Section title
        this.title = new SectionTitle('Informations')
        this.title.mesh.position.copy(dbWorld['INFORMATIONS'].position)
        this.title.mesh.position.y += 25

        // Informations
        this.informations = [
            new Information('itchio'),
            new Information('discord'),
            new Information('mail')
        ]
    }


    update() {
        if (this.informations) {
            this.informations.forEach((information) => {
                information.update()
            })
        }
    }
}