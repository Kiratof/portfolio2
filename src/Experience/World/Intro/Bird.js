import * as THREE from 'three'
import Experience from '../../Experience'

export default class Bird {
    constructor() {
        this.experience = new Experience()
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources
        this.time = this.experience.time
        this.debug = this.experience.debug

        // Debug
        if (this.debug.active) {
            this.debugFolder = this.debug.ui.addFolder('bird')
        }

        // Setup
        this.ressource = this.ressources.items.birdModel

        this.setModel()
        this.setAnimation()

        // Debug
        if (this.debug.active) {
            this.debugFolder.add(this.model.position, 'x').min(-1000).max(1000).step(1)
            this.debugFolder.add(this.model.position, 'y').min(-1000).max(1000).step(1)
            this.debugFolder.add(this.model.position, 'z').min(-1000).max(1000).step(1)
        }

    }


    setModel() {
        this.model = this.ressource.scene

        this.scene.add(this.model)

        this.model.traverse((child) => {
            if (child instanceof THREE.Mesh) {
                child.castShadow = true
            }
        })

    }

    setAnimation() {
        this.animation = {}
        this.animation.mixer = new THREE.AnimationMixer(this.model)

        this.animation.actions = {}
        this.animation.actions.flying = this.animation.mixer.clipAction(this.ressource.animations[0])

        this.animation.actions.current = this.animation.actions.flying
        this.animation.actions.current.play()


    }

    update() {
        this.animation.mixer.update(this.time.delta * 0.005)
    }
}