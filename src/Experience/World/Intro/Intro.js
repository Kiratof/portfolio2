import Experience from "../../Experience";
import Floor from "./Floor";
import SectionTitle from "../SectionTitle";
import dbWorld from "../../Database/dbWorld";
import PalmTree from "./PalmTree";
import Bird from "./Bird";
import Boat from "./Boat";
import Guy from "./Guy";

export default class Intro {
    constructor() {

        this.experience = new Experience()
        this.scene = this.experience.scene
        this.time = this.experience.time
        this.camera = this.experience.camera
        this.world = this.experience.world
        this.debug = this.experience.debug

        // Setup
        this.setTitle()
        this.setUpFloor()
        this.setUpGuy()
        this.setUpBird()
        this.setUpBoat()
        this.setUpPalmTrees()

        this.speed = 0.001

        // Debug
        if (this.debug.active) {
            this.debugFolder = this.debug.ui.addFolder('parralax')
            this.debugFolder.add(this, 'speed').min(0).max(0.3).step(0.00001)
        }

    }

    setTitle() {
        this.title = new SectionTitle('Chaotic Monkey')
        this.title.mesh.position.copy(dbWorld['INTRO'].position)
        this.title.mesh.position.x -= 20
        this.title.mesh.position.y += 13
    }

    setUpFloor() {
        this.floor = new Floor()
        this.floor.mesh.position.copy(dbWorld['INTRO'].position)
        this.floor.mesh.position.z = 490
        this.floor.mesh.position.y = -0.5
        this.floor.mesh.rotation.x = - Math.PI * 0.5
    }

    setUpGuy() {
        this.guy = new Guy()
        this.guy.model.scale.set(5, 5, 5)
        this.guy.model.rotation.y = Math.PI * 0.5
        this.guy.model.position.x -= 5
    }


    setUpBird() {
        this.bird = new Bird()
        this.bird.model.position.y = 50
        this.bird.model.position.z = -200
        this.bird.model.rotation.y = - Math.PI * 0.5
    }

    setUpBoat() {
        this.boat = new Boat()
        this.boat.model.rotation.y = - Math.PI * 0.5

        this.boat.model.position.x = 0
        this.boat.model.position.y = -1
        this.boat.model.position.z = -400
    }

    setUpPalmTrees() {

        this.palmTrees = []
        const palmCount = 10
        const palmTree = new PalmTree()


        palmTree.model.position.set(0, 5, 20)
        this.palmTrees.push(palmTree.model)


        for (let index = 1; index < palmCount; index++) {

            let randomOffset = 5 + Math.random() * 10
            let randomRotation = Math.random() * 0.5

            const clonedModel = palmTree.model.clone()
            clonedModel.position.x += index * randomOffset
            clonedModel.rotation.set(0, Math.PI * randomRotation, 0)

            this.palmTrees.push(clonedModel)
            this.scene.add(clonedModel)
        }

    }

    update() {

        if (this.boat) {
            if (this.boat.model.position.x > -750) {
                this.boat.model.position.x -= this.speed * this.time.delta
            } else {
                this.boat.model.position.x = 750
            }
        }

        if (this.bird) {
            this.bird.update()

            if (this.bird.model.position.x > -300) {
                this.bird.model.position.x -= this.speed * this.time.delta * 25
            } else {
                this.bird.model.position.x = 300
            }
        }

        if (this.palmTrees) {

            this.palmTrees.forEach(palmTree => {

                if (palmTree.position.x > -75) {
                    palmTree.position.x -= this.speed * this.time.delta
                } else {
                    palmTree.position.x = 75
                }
            })
        }

        if (this.guy) {
            this.guy.update()
        }
    }
}