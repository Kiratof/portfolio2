import Experience from '../../Experience'

export default class PalmTree {
    constructor() {
        this.experience = new Experience()
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources
        this.debug = this.experience.debug

        // Setup
        this.setModel()

        // Debug
        if (this.debug.active) {
            this.debugFolder = this.debug.ui.addFolder('Palm tree')
            this.debugFolder.add(this.model.position, 'x').min(-1000).max(1000).step(1)
            this.debugFolder.add(this.model.position, 'y').min(-1000).max(1000).step(1)
            this.debugFolder.add(this.model.position, 'z').min(-1000).max(1000).step(1)
        }

    }

    setModel() {
        this.model = this.ressources.items.palmTreeModel.scene
        this.model.scale.set(3, 3, 3)
        this.scene.add(this.model)
    }
}