import * as THREE from 'three'
import Experience from "../../Experience"
import dbColors from '../../Database/dbColors'

export default class Floor {
    constructor() {
        this.experience = new Experience()
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources
        this.time = this.experience.time

        this.debug = this.experience.debug

        // Debug
        this.setGeometry()
        this.setMaterial()
        this.setMesh()

        // Debug
        if (this.debug.active) {

            this.debugFolder = this.debug.ui.addFolder('Floor')
            this.debugFolder.add(this.mesh.position, 'x').min(-1000).max(1000).step(1)
            this.debugFolder.add(this.mesh.position, 'y').min(-1000).max(1000).step(1)
            this.debugFolder.add(this.mesh.position, 'z').min(-1000).max(1000).step(1)
        }

    }

    setGeometry() {
        this.geometry = new THREE.PlaneGeometry(1000, 1000, 2, 2)

    }

    setMaterial() {
        this.material = new THREE.MeshStandardMaterial(
            {
                color: new THREE.Color(`#${dbColors['Orange'].hex}`)
            }
        )
    }

    setMesh() {
        this.mesh = new THREE.Mesh(
            this.geometry,
            this.material
        )

        this.mesh.receiveShadow = true
        this.scene.add(this.mesh)
    }
}