import Experience from "../Experience";
import Environment from "./Environment";

import Projects from './Projects/Projects.js';
import Informations from './Informations/Informations.js';
import Intro from './Intro/Intro.js';
import dbWorld from '../Database/dbWorld';

export default class World {
    constructor() {
        this.experience = new Experience()
        this.scene = this.experience.scene
        this.ressources = this.experience.ressources

        // Wait for ressources
        this.ressources.on('ready', () => {

            // Setup
            this.intro = new Intro()

            this.projects = new Projects()

            this.informations = new Informations()

            this.environment = new Environment()
        })
    }

    update() {
        if (this.intro && dbWorld['INTRO'].active) {
            this.intro.update()
        }

        if (this.projects && dbWorld['PROJECTS'].active) {
            this.projects.update()
        }

        if (this.informations && dbWorld['INFORMATIONS'].active) {
            this.informations.update()
        }

        if (this.environment) {
            this.environment.update()
        }

    }
}