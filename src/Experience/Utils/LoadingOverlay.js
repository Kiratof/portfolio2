import * as THREE from 'three'
import { gsap } from 'gsap'
import Experience from '../Experience';

export default class LoadingOverlay {
    constructor() {
        this.experience = new Experience()
        this.scene = this.experience.scene

        this.loadingBarElement = document.querySelector('.loading-bar')

        /**
         * Overlay
         */
        this.overlayGeometry = new THREE.PlaneGeometry(2, 2, 1, 1)
        this.overlayMaterial = new THREE.ShaderMaterial({
            transparent: true,
            uniforms: {
                uAlpha: { value: 1 }
            },
            vertexShader: `

                void main() {
                    gl_Position = vec4(position, 1.0);
                }
            `,
            fragmentShader: `
        
                uniform float uAlpha;

                void main () {
                    gl_FragColor = vec4(0.0, 0.0, 0.0, uAlpha);
                }
            `
        })
        this.overlay = new THREE.Mesh(this.overlayGeometry, this.overlayMaterial)
        this.scene.add(this.overlay)
    }

    updateOverlay(loaded, toLoad) {

        const progressRatio = loaded / toLoad
        this.loadingBarElement.style.transform = `scaleX(${progressRatio})`

        if (loaded === toLoad) {

            gsap.delayedCall(0.5, () => {
                gsap.to(this.overlayMaterial.uniforms.uAlpha, { duration: 1, value: 0 })
                this.loadingBarElement.classList.add('ended')
                this.loadingBarElement.style.transform = ''
            })

        }
    }
}